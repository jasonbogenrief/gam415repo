// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PaintBlastBogenriefGameMode.generated.h"

UCLASS(minimalapi)
class APaintBlastBogenriefGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APaintBlastBogenriefGameMode();
};



